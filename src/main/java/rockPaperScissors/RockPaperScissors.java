package rockPaperScissors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public boolean is_winner(String choice1, String choice2) {
        // This method returns true if choice1 is the winner and false otherwise.
        if (choice1.equals("paper"))
            return choice2.equals("rock");
        else if (choice1.equals("scissors"))
            return choice2.equals("paper");
        else 
            return choice2.equals("scissors");
    }

    public boolean validate_input(String input, List <String> valid_input) {
        // This method validates that the answer given is acceptable.
        input = input.toLowerCase();
        return (valid_input.contains(input));
    }

    public String user_choice() {
        // This method asks the player if he would like to choose rock,
        // paper or scissors and then validates that nothing else is answered.
        while (true) {
            System.out.println("Your choice (Rock/Paper/Scissors)?");
            String hc = sc.next();
            if (validate_input(hc, rpsChoices))
                return hc;
            else
                System.out.println("I don't understand " + hc + ". Try again");
        }
    }

    public String continue_playing() {
        // Asks the player if they wish to continue playing and validates the answer.
        while (true) {
            System.out.println("Do you wish to continue playing? (y/n)?");
            String contans = sc.next();
            List <String> accepted_answers = new ArrayList<String>();
                accepted_answers.add("y");
                accepted_answers.add("n");
            if (validate_input(contans.toLowerCase(), accepted_answers))
                return contans;
            else
            System.out.println("I don't understand" + contans + ". Try again");
        }
    }

    public void run() {

        while (true) {
            System.out.println("Let's play round "+roundCounter);
            
            // Asking the player for their choice and makes an random computer choice:
            String human_choice = user_choice();
            java.util.Random random = new java.util.Random();
            int random_rps = random.nextInt(rpsChoices.size());
            String computer_choice = rpsChoices.get(random_rps);
            String choice_string = String.format("Human chose %1$s, computer chose %2$s", human_choice, computer_choice);

            // Checking who is the winner:
            if (is_winner(human_choice, computer_choice)) {
                System.out.println(choice_string + " Human wins!");
                humanScore++;
            }
            else if (is_winner(computer_choice, human_choice)) {
                System.out.println(choice_string + " Computer wins!");
                computerScore++;
            }
            else {
                System.out.println(choice_string + " It's a tie!");
            }

            // Showing scoreline and adding to round counter.
            System.out.println(String.format("Score: human %1$s, computer %2$s", humanScore, computerScore));
            roundCounter++;

            String continue_answer = continue_playing();
            if (continue_answer.equals("n")) {      // Breaks the loop if answer is "n"
                break;
            }
        }
        System.out.println("Bye bye :)");

    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
